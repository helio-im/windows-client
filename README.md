# Windows Client #
A Windows client for Helio using the .NET framework written in VB. Built upon WPF so that there is more ease when designing the application.

## Screenshots ##
![Capture.PNG](https://bitbucket.org/repo/Xxda4j/images/3146256152-Capture.PNG)

![Capture.PNG](https://bitbucket.org/repo/Xxda4j/images/2202302178-Capture.PNG)